# **triangle-light v0.1**

## **Purpose**
This project is an open-source IoT addressable triangluar accent light capable of controlling one or more light triangles using a low-cost ESP8266 and WS2812b LEDs.

![Triangle Light V0.1](images/triangle-lightv0.1.jpg)

## **v0.1 Features**
- Basic Web Interface to manually set light colors
- Ability to read configuration information from SPI FS file config.json
- Smooth slewing between colors
---

